#!groovy
import com.contus.cd.Constant

/**
 * Build APK
 * Bind Build information like Build number and Build date
 *
 * @param config
 * @return
 */
def call(Map config) {
    String environment = "${config.environment}".toLowerCase().tokenize().collect { it.capitalize() }.join('')
    String appName = "${config.appName}".toLowerCase()
    currentBuild.displayName = "#${BUILD_NUMBER} Build ${environment} APK"

    String version = ""
    String tag = ""

    if("${env.BRANCH_NAME}".toLowerCase().contains("release/")){
        version = "${env.BRANCH_NAME}".replace("*/", "")
        String[] tag1;
        tag1 = "${version}".split('release/');
        imageTag = "${tag1[1]}_${BUILD_NUMBER}"
    }else{
        tag = "${config.environment}_${BUILD_NUMBER}"
    }

    // Bind Build Number ans Date
    // bindBuildInfo module: "Android"
    // Copy and move the release information
    //sh ENV = 'qa'"cp -r releasenote.txt app/src/main/assets/releasenotes/"
    //sh "mv versionnote app/src/main/assets/releasenotes/"

    //if(!Constant.STATIC_ANALYSIS_ANDROID) {
        // Build APK
        sh "./gradlew clean assemble${environment}"
    //}

    //println("${appName}")

    if("${appName}".contains("mahindra")){
      String apkFile = "app/build/outputs/apk/${appName.replaceAll("\\s", "")}_${tag}.apk"
      sh "mv app/build/outputs/apk/${config.environment}/*-${config.environment}.apk ${apkFile}"
      //archiveArtifacts '**/*.apk'
    }else{
      String apkFile = "app/build/outputs/apk/${appName.replaceAll("\\s", "")}_${tag}.apk"
      sh "mv app/build/outputs/apk/${config.environment}/*.apk ${apkFile}"
      //archiveArtifacts '**/*.apk'
    }

    //pushArtifacts classifier: "android", file: "${apkFile}", ext: "apk", release: false
}