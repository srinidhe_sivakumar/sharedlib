#!groovy

/**
 * Get email content
 *
 * @param config
 * @return
 */
def call(Map config){
    return readTemplate().replace("%MAIN_CONTENT%", "${config.main}").replace("%SUB_CONTENT%", "${config.sub}")
            .replace("%TEAM%", "Build Team.");
}

def readTemplate(){
    def emailContent = libraryResource "templates/email_template.html"
    return emailContent
}
