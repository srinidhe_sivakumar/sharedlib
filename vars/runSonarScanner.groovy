#!groovy

import com.contus.cd.Constant

/**
 * Read the properties sonar-project.properties.
 * Create new project key and project name.
 * Load Sonarqube scanner tool from jenkins.
 * Run Sonarqube scanner, with the properties.
 *
 * @return none
 */
def call(Map config) {

    String environment = "Debug"

    if ("${config.environment}".length() > 0 && config.containsKey("environment")) {
        environment = "${config.environment}".toLowerCase().tokenize().collect { it.capitalize() }.join('')
    }

    if ("${config.platform}".toLowerCase().contains("java")) {
        sh "mvn clean install package"
    } else if ("${config.platform}".toLowerCase().contains("scala")) {
        sh "sbt assembly"
    } else if ("${config.platform}".toLowerCase().contains("android")) {
        sh "./gradlew clean assemble${environment}"
    }
        // Prepare Project Key from the branch name
        String key = "${env.BRANCH_NAME}".replace("*/", "").replace("/", "-");

        // Read and Load properties
        def sonarProp = readFile encoding: 'UTF-8', file: "${config.sonarProperties}"

        Properties propSonar = new Properties()
        propSonar.load(new StringReader(sonarProp))

        // Create new project key and project name.
        def sonarProjectKey = propSonar.getProperty("sonar.projectKey") + "_" + key.toUpperCase()
        def sonarProjectName = propSonar.getProperty("sonar.projectName") + "-" + key
        def sonarSources = propSonar.getProperty("sonar.sources")
        def workdir = propSonar.getProperty("sonar.workdir")

        // Setup the Sonarqube project key in the Constant which can be used in the same pipeline
        Constant.SONAR_PROJECT_KEY = sonarProjectKey

        withDockerContainer(args: "--entrypoint=''", image: 'newtmitch/sonar-scanner') {
            sh "/usr/local/bin/sonar-scanner -Dsonar.projectBaseDir=$WORKSPACE -Dproject.settings=sonar-project.properties -Dsonar.projectKey=${sonarProjectKey} -Dsonar.projectName=${sonarProjectName}"
        }

        if("${config.platform}".toLowerCase().contains("java") || "${config.platform}".toLowerCase().contains("android")) {
            recordIssues(tools: [findBugs(pattern: '**/findbugs-result.xml', useRankAsPriority: true), checkStyle(pattern: '**/checkstyle-result.xml')])
        }
        // Load SonarQube Scanner Tool from Jenkins
        def sonarqubeScannerHome = tool name: 'sonarscanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation'

         //Run SonarQube Scanner "-X -Dsonar.verbose=true"
         //sh "${sonarqubeScannerHome}/bin/sonar-scanner -Dproject.settings=sonar-project.properties " +
         //       "-Dsonar.projectKey=${sonarProjectKey} -Dsonar.projectName='${sonarProjectName}'"

    }