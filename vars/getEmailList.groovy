#!groovy

/**
 * Get the email list from the dev properties file.
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the platform name
    String module = "${config.module}".toLowerCase()

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${module}/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    def opsEmail = pipelineProps['pipeline.email.ops']
    def devEmail = pipelineProps['pipeline.email.dev']

    return "${opsEmail}, ${devEmail}"

}



