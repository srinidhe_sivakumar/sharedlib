#!groovy

/**
 * Create or update the docker service.
 * Create volume and mount to the docker service. (only for magento)
 * Remove old docker image.
 *
 *
 * @return
 */
def call(Map config) {
    /**
     * Docker Service Implementation and Update
     * Roll out New Version
     */

    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""
    String dockerConfig = ""

    if (config.isDeploy != null && config.isDeploy) { // For deployment dev
        imageTag = config.imageTag
    } else {
        if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
            version = "${BRANCH_NAME}".replace("*/", "")
            String[] tag;
            tag = "${version}".split('release/');
            imageTag = "${tag[1]}_${BUILD_NUMBER}"
        } else {
            imageTag = "${BUILD_NUMBER}"
            imageName = imageName + "_dev"
        }
    }

    def appName = "mymtb"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    //Image name
    imageName = "${imageName}:${imageTag}"

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${config.module}/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    if (config.isConfigExist) {
        dockerConfig = getExistingConfig("${pipelineProps['pipeline.deploy.stack.service']}")
    } else {
        dockerConfig = "${appName}-${config.module}-env"
    }

    // Remove the last version docker image
    String serviceImageName = sh returnStdout: true, script: "docker service ls --filter " +
            "name=${pipelineProps['pipeline.deploy.stack.service']} --format " + "\"{{.Image}}\""

    def nodedockerYaml = libraryResource "all/${config.module}.yml"
    nodedockerYaml = nodedockerYaml.replace("%IMAGE%", "${imageName}").replace("%CONFIG_NAME%", "${dockerConfig}")
            .replace ("%CONFIG_PATH%", "${pipelineProps['pipeline.config.env.target']}")

    // Create modified yaml file
    writeFile encoding: 'UTF-8', file: "${config.module}.yml", text: "${nodedockerYaml}"

    //Stack deploy
    sh "docker stack deploy --compose-file ${config.module}.yml ${pipelineProps['pipeline.deploy.stack.name']} --with-registry-auth"


    if (serviceImageName.length() != 0) {
        // Sleep 20 Seconds
        sleep 20;

        sh("docker rmi -f ${serviceImageName}");
    }

    currentBuild.result = "SUCCESS"

}

String getExistingConfig(String serviceName){
    String configName = sh returnStdout: true, script: "docker service inspect ${serviceName} " +
            "--format='{{ (index (.Spec.TaskTemplate.ContainerSpec.Configs) 0).ConfigName}}'"

    if (configName.length() != 0) {
        return configName.trim()
    }else{
        return ""
    }
}