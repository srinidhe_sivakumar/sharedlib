#!groovy

/**
 * Check if the config already exist
 * Create config if not exist
 *
 * @return boolean is Config Exist
 */
def call(Map config) {

    String SERVICE_NAME = "${config.serviceName}"

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Get existing configuration
    String existingConfig = getExistingConfig("${appName}-${config.module}", config.isApache)
    println("oldDockerConfig: ${existingConfig}")

    /**
     * Create New Configuration
     */
    String newDockerConfig = "${appName}-${config.module}-env-${BUILD_NUMBER}"


    def envFileContent = ""

    if (config.isPackaging != null && config.isPackaging) {
        // Get the packaging env
        envFileContent = libraryResource "${appName}/${module}/packaging/${config.environment}"
    }else if (config.isApache != null && config.isApache) {
        // Get the Apache env
        envFileContent = libraryResource "${appName}/${config.module}/apache/${config.environment}"
        newDockerConfig = "${appName}-${config.module}-apache-env-${BUILD_NUMBER}"
    }else{
        // Load or read the resource env content
        envFileContent = libraryResource "${appName}/${module}/${config.environment}"
    }

        // Create a file env in the workspace
    writeFile encoding: 'UTF-8', file: 'config', text: "${envFileContent}"
    sh "docker config create ${newDockerConfig} config"

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${config.module}/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    // Prepare config source and target
    def addDockerConfig = "--config-add source=${newDockerConfig},target=${pipelineProps['pipeline.config.env.target']}"

    if (config.isPackaging != null && config.isPackaging) {
        addDockerConfig = "--config-add source=${newDockerConfig},target=${pipelineProps['pipeline.packaging.config.env.target']}"
    }

    if (config.isApache != null && config.isApache) {
        addDockerConfig = "--config-add source=${newDockerConfig},target=${pipelineProps['dev.apache.config.env.target']}"
    }

    String dockerConfig = addDockerConfig
    if (existingConfig.length() != 0) {
        dockerConfig = "--config-rm ${existingConfig} ${addDockerConfig}"
    }
    try {
        // Service update with new config and remove old config
        sh("docker service update ${dockerConfig} ${SERVICE_NAME} --detach=false")
        if (existingConfig.length() != 0) {
            sh("docker config rm ${existingConfig}")
        }
    }
    catch (java.lang.Exception e) {
        currentBuild.result = "FAILURE"
        error "Build failed: ${e.toString()}"
    }

    // Build Status
    currentBuild.result = "SUCCESS"
}


String getExistingConfig(String module, Boolean isApache){

    print ("isApache:" + isApache)

    if (isApache != null && isApache) {
        module = module + "-apache"
    }

    String configName = sh returnStdout: true, script: "docker config ls --filter=name=${module}-env " +
            "--format \"{{.Name}}\""

    if (configName.length() != 0) {
        return configName.trim()
    }else{
        return ""
    }
}
