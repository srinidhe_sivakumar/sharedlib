#!groovy

/**
 * Run selenium test base on the platform
 * Read the dev properties (Repo url and branch) of the platform
 *
 * @param config (platform)
 * @return none
 */
def call(Map config){

    def module = "${config.module}".toLowerCase()

    def appName = "iot"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${module}/dev.properties"

    def pipelineProps = readProperties text: "${pipelinePropText}"

    sh "mvn clean install package"

}