#!groovy

/**
 * Run setup shell script for packing software
 *
 * @param config
 * @return
 */
def call(Map config) {

    def appName = "iot"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Create setup file in the parent directory
    def setupContent = libraryResource "${appName}/${config.module}/shell/setup"
    writeFile encoding: 'UTF-8', file: 'setup.sh', text: "${setupContent}"

    if ("${config.module}".contains("api") || "${config.module}".contains("streamprocessor")) {
        def specContent = libraryResource "${appName}/${config.module}/packaging/versa.spec"
        writeFile encoding: 'UTF-8', file: 'versa1.spec', text: "${specContent}"

        // Load or read the resource env content
        def envFileContent = libraryResource "${appName}/${config.module}/${config.environment}"
        // Create a file env in the workspace
        writeFile encoding: 'UTF-8', file: '.env', text: "${envFileContent}"
    }

    // Change mode and Run setup
    sh "chmod +x setup.sh && ./setup.sh"

}



