#!groovy

/**
 * Upload artifacts to Nexus Repository
 *
 * @param config
 * @return
 */
def call(Map config) {

    nexusArtifactUploader artifacts: [[artifactId: 'versa_apk', classifier: 'android', file: 'env', type: 'env']],
            credentialsId: 'Jenkins-Nexus', groupId: 'com.versanetworks', nexusUrl: 'nexus-versa.contus.us:8081',
            nexusVersion: 'nexus3', protocol: 'http', repository: 'maven-snapshots/', version: '1.0-SNAPSHOT'

}



