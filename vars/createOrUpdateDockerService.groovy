#!groovy

/**
 * Create or update the docker service.
 * Create volume and mount to the docker service. (only for magento)
 * Remove old docker image.
 *
 *
 * @return
 */
def call(Map config) {
    /**
     * Docker Service Implementation and Update
     * Roll out New Version
     */

    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""

    if (config.isDeploy != null && config.isDeploy) { // For deployment dev
        imageTag = config.imageTag
    } else {
        if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
            version = "${BRANCH_NAME}".replace("*/", "")
            String[] tag;
            tag = "${version}".split('release/');
            imageTag = "${tag[1]}_${BUILD_NUMBER}"
        } else {
            imageTag = "${BUILD_NUMBER}"
            imageName = imageName + "_dev"
        }
    }

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    String SERVICE_NAME = "${config.serviceName}"
    String serviceName = sh returnStdout: true, script: "docker service ls --filter name=${SERVICE_NAME} --format " +
            "\"{{.Name}}\""

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${config.module}/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    // Prepare config source and target
    def addConfig = "source=${appName}-${config.module}-env,target=${pipelineProps['pipeline.config.env.target']}"

    // Prepare config source and target
    def addApacheConfig = "source=${appName}-${config.module}-apache-env,target=${pipelineProps['dev.apache.config.env.target']}"

    /**
     * Create and Mount Volume
     */
    String mountVolume = ""
    String mountVolume2 = ""
    String mountVolume3 = ""

    def replicas = "2"
    if ("${config.module}".contains("shop")) {
        //replicas = "1"
        String volumeName = sh returnStdout: true, script: "docker volume ls --filter=name=magento-media " +
                "--format \"{{.Name}}\""

        if (volumeName.length() == 0) {
            sh "docker volume create magento-media"
        }

        mountVolume = "--mount type=volume,source=magento-media,target=/var/www/html/shop/pub/media "

        /**
         * Check and create reseller volume for magento
         */
        String logsVolumeName = sh returnStdout: true, script: "docker volume ls --filter=name=magento-reseller " +
                "--format \"{{.Name}}\""

        if (logsVolumeName.length() == 0) {
            sh "docker volume create magento-reseller"
        }

        mountVolume2 = "--mount type=volume,source=magento-reseller,target=/var/www/html/shop/reseller "

        /**
         * Check and create static volume for magento
         */
        String staticVolumeName = sh returnStdout: true, script: "docker volume ls --filter=name=magento-static " +
                "--format \"{{.Name}}\""

        if (staticVolumeName.length() == 0) {
            sh "docker volume create magento-static"
        }

        mountVolume3 = "--mount type=volume,source=magento-static,target=/var/www/html/shop/pub/static/_cache "

    } else if ("${config.module}".contains("api")) {
        String volumeName = sh returnStdout: true, script: "docker volume ls --filter=name=api-logs " +
                "--format \"{{.Name}}\""

        if (volumeName.length() == 0) {
            sh "docker volume create api-logs"
        }

        mountVolume = "--mount type=volume,source=api-logs,target=/home/docker/code/app/logs "
    }else if ("${config.module}".contains("operator")) {
        String volumeName = sh returnStdout: true, script: "docker volume ls --filter=name=operator-logs " +
                "--format \"{{.Name}}\""

        if (volumeName.length() == 0) {
            sh "docker volume create operator-logs"
        }

        mountVolume = "--mount type=volume,source=operator-logs,target=/home/docker/code/app/logs "
    }

    // Setup replicas using config params, if its deploy dev
    if (config.replicas != null && config.isDeploy) {
        replicas = "${config.replicas}"
    }

    String serviceImageName = sh returnStdout: true, script: "docker service ls --filter " +
            "name=${SERVICE_NAME} --format " + "\"{{.Image}}\""

    /**
     * Create Proxy Network if not exist
     */
    String networkName = sh returnStdout: true, script: "docker network ls --filter name=proxy --format \"{{.Name}}\""
    if (!networkName.trim().equals("proxy")) {
        sh "docker network create --driver overlay proxy"
    }

    def dockerConfig = ""

    // If Service Name exist the Update Service
    if (serviceName.trim().equals(SERVICE_NAME)) {

        if (config.isConfigExist) {
            dockerConfig = ""
        } else {
            dockerConfig = "--config-add ${addConfig}"
        }

        if (config.containsKey("isApacheConfigExist") && !config.isApacheConfigExist) {
            dockerConfig = " --config-add ${addApacheConfig}"
        }

        if (mountVolume != null && mountVolume.length() > 0) {
            mountVolume = mountVolume.replace("--mount", "--mount-add")
        }

        if (mountVolume2 != null && mountVolume2.length() > 0) {
            mountVolume2 = mountVolume2.replace("--mount", "--mount-add")
        }

        if (mountVolume3 != null && mountVolume3.length() > 0) {
            mountVolume3 = mountVolume3.replace("--mount", "--mount-add")
        }

        if ("${config.module}".contains("proxy")) {
            replicas = "1"
        }

        sh("docker service update --image ${imageName}:${imageTag} ${SERVICE_NAME} --detach=false " +
                "${dockerConfig} ${mountVolume} ${mountVolume2} ${mountVolume3} --with-registry-auth --update-delay " +
                "10s")
    } else {
        // Get the old container name. This is for the first time
        String oldContainerName = sh returnStdout: true, script: "docker ps --filter=name=${SERVICE_NAME} " +
                "--format \"{{.Names}}\""

        //If old container name available Remove it
        if (oldContainerName.length() != 0) {
            //sh("docker rm -f ${oldContainerName}");
        }


        if (config.isConfigExist) {
            String configName = sh returnStdout: true, script: "docker config ls --filter name=${appName}-${config.module}-env " +
                    "--format \"{{.Name}}\""
            dockerConfig = "--config source=${configName.trim()},target=${pipelineProps['pipeline.config.env.target']}"
        } else {
            dockerConfig = "--config ${addConfig}"
        }

        if (config.isApacheConfigExist) {
            String configName = sh returnStdout: true, script: "docker config ls --filter name=${appName}-${config.module}-apache-env " +
                    "--format \"{{.Name}}\""
            dockerConfig = "--config source=${configName.trim()},target=${pipelineProps['dev.apache.config.env.target']}"
        } else {
            dockerConfig = "--config ${addConfig}"
        }

        if ("${config.module}".contains("proxy")) {
            replicas = "1"
            // If Service Name does not exist Create Service
            sh("docker service create --name ${SERVICE_NAME} ${dockerConfig} " +
                    "${mountVolume}--publish mode=host,target=80,published=80 --mode global --network proxy " +
                    "--log-driver " +
                    "syslog --log-opt syslog-address=\"udp://127.0.0.1:514\" --log-opt tag=\"${SERVICE_NAME}/{{" +
                    ".ImageName}}/{{.ID}}\" --log-opt syslog-facility=${pipelineProps['docker.syslog.facility']} " +
                    "--log-opt syslog-format=rfc5424micro " +
                    "--with-registry-auth ${imageName}:${imageTag}")
        } else {

            // If Service Name does not exist Create Service
            sh("docker service create --name ${SERVICE_NAME} ${dockerConfig} --replicas ${replicas} " +
                    "${mountVolume} ${mountVolume2} ${mountVolume3}--network proxy --network go --log-driver syslog " +
                    "--log-opt syslog-address=\"udp://127.0.0.1:514\" --log-opt tag=\"${SERVICE_NAME}/{{.ImageName}}/{{.ID}}\" " +
                    "--log-opt syslog-facility=${pipelineProps['docker.syslog.facility']} --log-opt " +
                    "syslog-format=rfc5424micro --with-registry-auth " +
                    "${imageName}:${imageTag}")
        }
    }

    if (config.isDeploy == null || !config.isDeploy) {
        // Remove the last version docker image
        if (serviceImageName.length() != 0) {
            // Sleep 10 Seconds
            sleep 10;

            sh("docker rmi -f ${serviceImageName}");
        }
    }

    currentBuild.result = "SUCCESS"

}