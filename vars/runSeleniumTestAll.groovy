#!groovy

/**
 * Run selenium test base on the platform
 * Read the dev properties (Repo url and branch) of the platform
 *
 * @param config (platform)
 * @return none
 */
def call(Map config){

    def sureFire = ""

    def ignore = true
    if(config.containsKey("failBuild")) {
        ignore = !config.failBuild
    }

    if(config.containsKey("environment")){
        String env = "${config.environment}".toLowerCase()
        sureFire = " -Dsurefire.suiteXmlFiles=${env}.xml"
    }

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Load or read the Dashboard resource dev properties content
    String pipelinePropText = libraryResource "${appName}/dashboard/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    sh 'mkdir -p selenium-dashboard'
    dir("selenium-dashboard") {
        git url:"${pipelineProps['pipeline.test.repo_url']}", branch: "${pipelineProps['pipeline.test.repo_branch']}",
                credentialsId: "sathish_bitbucket"

        sh "mvn -Dmaven.test.failure.ignore=${ignore}${sureFire} clean install"
    }

    // Load or read the Shop resource dev properties content
    String pipelinePropText1 = libraryResource "${appName}/shop/dev.properties"
    def pipelineProps1 = readProperties text: "${pipelinePropText1}"

    sh 'mkdir -p selenium-shop'
    dir("selenium-shop") {
        git url:"${pipelineProps1['pipeline.test.repo_url']}", branch: "${pipelineProps1['pipeline.test.repo_branch']}",
                credentialsId: "sathish_bitbucket"

        sh "mvn -Dmaven.test.failure.ignore=${ignore}${sureFire} clean install"
    }

}

