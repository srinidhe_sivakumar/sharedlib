#!groovy
/**
 * Push docker image to the nexus repository
 */
def call(Map config) {
    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""

    if("${env.BRANCH_NAME}".toLowerCase().contains("release/")){
        version = "${env.BRANCH_NAME}".replace("*/", "").replace("release/", "")
        imageTag = "${version}_${BUILD_NUMBER}"
    }else{
        imageTag = "${BUILD_NUMBER}"
        imageName = imageName + "_dev"
    }

    sh "docker push ${imageName}:${imageTag}"
}