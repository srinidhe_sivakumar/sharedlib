#!groovy

/**
 * Rebuild docker service.
 * This class is not currently used.
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the docker name
    String serviceName = "${config.serviceName}".toLowerCase()


    // Get service image name
    String serviceImageName = sh returnStdout: true, script: "docker service ls --filter " +
            "name=${serviceName} --format " + "\"{{.Image}}\""

    sh("docker service update ${serviceName} --image ${serviceImageName} --detach=false")
}



