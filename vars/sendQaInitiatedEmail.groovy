#!groovy

/**
 * This method is used to send the QA process initiation email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    def emailContent = getEmailContent main: "We have initiated the QA process and will update the status within " +
            "${config.value} Business ${config.metric}.", sub: ""

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "all/pipeline.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    emailext attachLog: false, body: "${emailContent}",
            subject: 'QA Process - $PROJECT_NAME - Build #$BUILD_NUMBER', to: "${pipelineProps['pipeline.email.qa']}, ${config.to}"
}