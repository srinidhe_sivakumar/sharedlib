#!groovy

/**
 * Check if the config already exist
 * Create config if not exist
 *
 * @return boolean is Config Exist
 */
def call(Map config) {

    String SERVICE_NAME = "${config.serviceName}"

    // Get existing configuration
    String existingConfig = getExistingConfig("${config.module}")
    println("oldDockerConfig: ${existingConfig}")

    /**
     * Create New Configuration
     */
    String newDockerConfig = "${config.module}-whitelist-${BUILD_NUMBER}"

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Load or read the resource env content
    def envFileContent = libraryResource "${appName}/${config.module}/whitelist/${config.environment}"

    // Create a file env in the workspace
    writeFile encoding: 'UTF-8', file: 'config', text: "${envFileContent}"
    sh "docker config create ${newDockerConfig} config"

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${config.module}/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    // Prepare config source and target
    def addDockerConfig = "--config-add source=${newDockerConfig},target=${pipelineProps['pipeline.config.whitelist.target']}"

    String dockerConfig = addDockerConfig
    if (existingConfig.length() != 0) {
        dockerConfig = "--config-rm ${existingConfig} ${addDockerConfig}"
    }
    try {
        // Service update with new config and remove old config
        sh("docker service update ${dockerConfig} ${SERVICE_NAME} --detach=false")
        if (existingConfig.length() != 0) {
            sh("docker config rm ${existingConfig}")
        }
    }
    catch (java.lang.Exception e) {
        currentBuild.result = "FAILURE"
        error "Build failed: ${e.toString()}"
    }

    // Build Status
    currentBuild.result = "SUCCESS"
}


String getExistingConfig(String module){
    String configName = sh returnStdout: true, script: "docker config ls --filter=name=${module}-whitelist " +
            "--format \"{{.Name}}\""

    if (configName.length() != 0) {
        return configName.trim()
    }else{
        return ""
    }
}
