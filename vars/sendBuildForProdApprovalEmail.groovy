#!groovy

/**
 * This method is used to send the Build QA Approval email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    def emailContent = getEmailContent main: "The Build is ready for Production process. Approve the build and" +
            " send it for Production", sub: "Pipeline URL: ${JOB_URL}"

    emailext attachLog: false, body: "${emailContent}", subject: '🔵 Approve the Build for Production process - ' +
            '$PROJECT_NAME - Build #$BUILD_NUMBER - $BUILD_STATUS', to: "${config.to}"
}