#!groovy

/**
 * NPM install
 *
 * @param config
 * @return
 */
def install(Map config) {
    def params = ""
    if (config.containsKey("unsafe_perm") && config.unsafe_perm) {
        params += " --unsafe-perm=true"
    }

    if (config.containsKey("allow_root") && config.allow_root) {
        params += " --allow-root"
    }

    sh "npm install ${params}"
}

/**
 * NPM run build
 *
 * @param config
 * @return
 */
def run_build(Map config=[:]){
    sh "npm run build"
}



