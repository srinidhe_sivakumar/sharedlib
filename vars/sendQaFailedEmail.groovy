#!groovy

/**
 * This method is used to send the QA process failed email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    def emailContent = getEmailContent main: "QA FAILED - ${JOB_NAME} #${BUILD_NUMBER}. Kindly fix the bugs and " +
            "revert for QA Process.", sub: "Pipeline URL: ${JOB_URL}"

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "all/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    emailext attachLog: false, body: "${emailContent}", subject: 'QA Process - $PROJECT_NAME - Build #' +
            '$BUILD_NUMBER', to: "${pipelineProps['pipeline.email.qa']}, ${config.to}"
}