#!groovy
import com.contus.cd.Constant

/**
 * Run swift lint.
 * Publish and send report
 *
 * @param config
 * @return
 */
def call(Map config) {

    Constant.XCODE_PROJECT_NAME = "${config.XCODE_PROJECT_NAME}"

    // Remove node_module folder
    sh "rm -rf node_modules/"

    //Prepare exclusions
    String exclusions = prepareExclusions()

    sh "echo 'whitelist_rules:\n" +
            "excluded: # paths to ignore during linting. Takes precedence over `included`.\n" +
            "${exclusions}" +
            "# Disable rules\n" +
            "disabled_rules:\n" +
            "  - multiple_closures_with_trailing_closure\n" +
            "  - notification_center_detachment\n" +
            "# rules that have both warning and error levels, can set just the warning level\n" +
            "# implicitly\n" +
            "line_length: 150\n" +
            "# they can set both implicitly with an array\n" +
            "cyclomatic_complexity:\n" +
            "  warning: 20\n" +
            "  error: 20\n" +
            "large_tuple: 8\n" +
            "reporter: \"checkstyle\" # reporter type (xcode, json, csv, checkstyle)' > .swiftlint.yml"

    boolean lint = sh returnStdout: true, script: "/usr/local/bin/swiftlint lint --reporter " +
            "checkstyle > report.xml || true"

    String report = readFile encoding: 'UTF-8', file: "report.xml"

    if (report.contains("<file")) {
        lint = false
    } else {
        lint = true
    }
    println("Lint: ${lint}")

    checkstyle canComputeNew: false, defaultEncoding: 'UTF-8', healthy: '', pattern: 'report.xml', unHealthy: ''

    if (!lint) {
        currentBuild.result = "FAILED"
        currentBuild.description = "Static Analysis Swift lint Failed"

        def emailContent = getEmailContent main: "The Build # $BUILD_NUMBER has been FAILED in SwiftLint check, " +
                "kindly fix it ASAP.", sub: "Report URL: ${JOB_URL}${BUILD_NUMBER}/checkstyleResult"

        // Send failure email with Sonarqube link attached
        emailext body: "${emailContent}",
                subject: '🔴 $PROJECT_NAME - Build # $BUILD_NUMBER - Static Analysis $BUILD_STATUS', recipientProviders:
                [[$class: 'CulpritsRecipientProvider'], [$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                to: "${config.to}"

        error "Static Analysis Swift lint Failed"
    }
}

/**
 * Prepare exclusion for Swift Lint
 * @return exclusion string
 */
String prepareExclusions() {
    def defaultExclusions = "  - ${Constant.XCODE_PROJECT_NAME}/Carthage\n" +
            "  - ${Constant.XCODE_PROJECT_NAME}/Pods\n"
    def otherExclusions = ""

    // Check properties file exists
    def checkFileExist = fileExists "${Constant.SWIFTLINT_PROPERTIES}"
    if (checkFileExist) {
        String swiftLintProperties = readFile encoding: 'UTF-8', file: "${Constant.SWIFTLINT_PROPERTIES}"

        Properties propSwiftLint = new Properties();
        // load a properties string
        propSwiftLint.load(new StringReader(swiftLintProperties));

        String[] excluded = propSwiftLint.getProperty("swiftlint.exclusions").split(',')

        for (String value : excluded) {
            otherExclusions += "  - ${value.trim()}\n"
        }
    }

    return "${defaultExclusions}${otherExclusions}"
}



