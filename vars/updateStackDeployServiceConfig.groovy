#!groovy

/**
 * Check if the config already exist
 * Create config if not exist
 *
 * @return boolean is Config Exist
 */
def call(Map config) {

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Get existing configuration
    String existingConfig = getExistingConfig("${appName}-${config.module}")
    println("oldDockerConfig: ${existingConfig}")

    /**
     * Create New Configuration
     */
    String dockerConfig = "${appName}-${config.module}-env-${BUILD_NUMBER}"


    // Load or read the resource env content
    def envFileContent = libraryResource "${appName}/${config.module}/${config.environment}"

    // Create a file env in the workspace
    writeFile encoding: 'UTF-8', file: 'config', text: "${envFileContent}"
    sh "docker config create ${dockerConfig} config"

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${config.module}/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"


    String imageName = sh returnStdout: true, script: "docker stack services ${pipelineProps['pipeline.deploy.stack.name']} --filter" +
            " name=${pipelineProps['pipeline.deploy.stack.service']} --format " + "\"{{.Image}}\""

    def nodedockerYaml = libraryResource "all/${config.module}.yml"
    nodedockerYaml = nodedockerYaml.replace("%IMAGE%", "${imageName}").replace("%CONFIG_NAME%", "${dockerConfig}")
            .replace("%CONFIG_PATH%", "${pipelineProps['pipeline.config.env.target']}")

    // Create modified yaml file
    writeFile encoding: 'UTF-8', file: "${config.module}.yml", text: "${nodedockerYaml}"

    //Stack deploy
    sh "docker stack deploy --compose-file ${config.module}.yml ${pipelineProps['pipeline.deploy.stack.name']} --with-registry-auth"

    try {
        if (existingConfig.length() != 0) {
            sh("docker config rm ${existingConfig}")
        }
    }
    catch (java.lang.Exception e) {
        currentBuild.result = "FAILURE"
        error "Build failed: ${e.toString()}"
    }

    currentBuild.result = "SUCCESS"
}

String getExistingConfig(String module) {
    String configName = sh returnStdout: true, script: "docker config ls --filter=name=${module}-env " +
            "--format \"{{.Name}}\""

    if (configName.length() != 0) {
        return configName.trim()
    } else {
        return ""
    }
}
