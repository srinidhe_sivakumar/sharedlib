#!groovy

/**
 * Pull docker image from the nexus repository
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""


    // For deployment dev
    if(config.isDeploy != null && config.isDeploy){
        imageTag = config.imageTag
    }else {
        if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
            version = "${BRANCH_NAME}".replace("*/", "")
            String[] tag;
            tag = "${version}".split('release/');
            imageTag = "${tag[1]}_${BUILD_NUMBER}"
        } else {
            imageTag = "${BUILD_NUMBER}"
            imageName = imageName + "_dev"
        }
    }
    docker.withRegistry('http://nexus-versa.contus.us:8083', 'nexus-versa-credentials') {
        sh "docker pull ${imageName}:${imageTag}"
    }
}



