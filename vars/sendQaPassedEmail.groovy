#!groovy

/**
 * This method is used to send the QA passed email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    def emailContent = getEmailContent main:"QA PASSED - ${JOB_NAME} #${BUILD_NUMBER}. Now the build is ready to " +
            "deploy in UAT", sub: "Pipeline URL: ${JOB_URL}"

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "all/pipeline.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    emailext attachLog: false, body: "${emailContent}", subject: 'QA Process - $PROJECT_NAME - Build #' +
            '$BUILD_NUMBER', to: "${pipelineProps['pipeline.email.qa']}, ${config.to}"
}

