#!groovy

/**
 * Pull docker image from the nexus repository
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""


    // For deployment dev
    if(config.isDeploy != null && config.isDeploy){
        imageTag = config.imageTag
    }else {
        if ("${env.BRANCH_NAME}".toLowerCase().contains("release/")) {
            version = "${env.BRANCH_NAME}".replace("*/", "")
            String[] tag;
            tag = "${version}".split('release/');
            imageTag = "${tag[1]}_${BUILD_NUMBER}"
        } else {
            imageTag = "${BUILD_NUMBER}"
            imageName = imageName + "_dev"
        }
    }
    docker.withRegistry('http://192.168.8.247:8083/', '6cb5a078-45e7-48d3-bfe0-3e106d34845d') {
        sh "docker pull ${imageName}:${imageTag}"
    }
}