#!groovy

import com.contus.cd.Constant

/**
 * Pull Artifacts to nexus repository
 *
 * @param config - version, type, classifier, dstFileName
 * @return
 */
def call(Map config) {
    String version = ""

    if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
        version = "${BRANCH_NAME}".replace("*/", "")
        String[] tag;
        tag = "${version}".split('release/');
        imageTag = "${tag[1]}_${BUILD_NUMBER}"
    } else {
        version = "${BUILD_NUMBER}-SNAPSHOT"
    }

    // Modify pom.xml
    def pomXML = libraryResource "maven/pom.xml"
    pomXML = "${pomXML}".replace("%VERSION%","${version}")
                        .replace("%TYPE%", "${config.type}")
                        .replace("%CLASSIFIER%", "${config.classifier}")
                        .replace("%FILENAME%", "${config.dstFileName}")

    writeFile encoding: 'UTF-8', file: 'pom.xml', text: "${pomXML}"

    // Get settings.xml
    def settingsXML = libraryResource "maven/settings.xml"
    writeFile encoding: 'UTF-8', file: 'settings.xml', text: "${settingsXML}"

    // Create directory (if not exist), to store artifacts
    sh "mkdir -p artifacts"

    // Clean artifacts directory
    sh "rm -rf artifacts/*"

    //Maven command to copy artifacts to a directory
    sh "mvn dependency:copy -s settings.xml"
}



