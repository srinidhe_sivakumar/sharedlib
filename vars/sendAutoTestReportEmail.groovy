#!groovy

/**
 * This method is used to send the auto test report email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    def emailContent = getEmailContent main: "Auto Test Report for ${JOB_NAME} #${BUILD_NUMBER}. Kindly find the " +
            "attachment.", sub: "Report URL: ${JOB_URL}${BUILD_NUMBER}/testReport"

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${config.module}/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    String env = "QA"
    if(config.containsKey("environment")){
        env = "${config.environment}".toUpperCase()
    }


    emailext attachmentsPattern: '**/Scheduled-Rpt-*.html', attachLog: false, body: "${emailContent}", subject: "Test " +
            "Rpt - ${config.service} - Release #${BUILD_NUMBER} - ${env} - ${getDate()} - ${config.status}", to:
            "${pipelineProps['pipeline.email.qa']}, ${config.to}"
}


/**
 * Get the current date
 * @return def Date
 */
def getDate() {
    def date = new Date()
    return date.format("MM/dd/yyyy hh:mm:ss z")
}