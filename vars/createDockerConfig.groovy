#!groovy

/**
 * Check if the config already exist
 * Create config if not exist
 *
 * @return boolean is Config Exist
 */
def call(Map config) {

    print ("isApache1: "+ config.isApache)

    def appName = "iot"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Check if docker config exist
    // Create config is not exist
    if (!isConfigExist("${config.module}", config.isApache, "${appName}")) {
        def module = "${config.module}".toLowerCase()



        def envFileContent = ""

        if (config.isPackaging != null && config.isPackaging) {
            // Get the packaging env
            envFileContent = libraryResource "${appName}/${module}/packaging/${config.environment}"
        }else if (config.isApache != null && config.isApache) {
            // Get the Apache env
            envFileContent = libraryResource "${appName}/${config.module}/apache/${config.environment}"
            module = module + "-apache"
        }else{
            // Load or read the resource env content
            envFileContent = libraryResource "${appName}/${module}/${config.environment}"
        }

        // Create a file env in the workspace
        writeFile encoding: 'UTF-8', file: 'env', text: "${envFileContent}"

        // Create config
        sh "docker config create ${appName}-${module}-env env"

        return false
    }

    return true
}

/**
 * 1. Check if docker config exist
 * 2. Create config if not exist
 *
 * @param environment
 */
boolean isConfigExist(String module, Boolean isApache, String appName) {
    module = module.toLowerCase()

    print ("isApache1:" + isApache)

    if (isApache != null && isApache) {
        module = module + "-apache"
    }

    String configName = sh returnStdout: true, script: "docker config ls --filter=name=${appName}-${module}-env " +
            "--format \"{{.Name}}\""

    if (configName.length() == 0) {
        return false
    } else {
        return true
    }
}
