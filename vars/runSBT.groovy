#!groovy

/**
 * Run SBT packaging command to build JAR for Scala project
 *
 * @param config (platform)
 * @return none
 */
def call(Map config){
    sh "sbt assembly"
}