import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.Cipher

/**
 * Created by Sathish on 2/13/2018.
 */

println("Test: AES Crypto")

String plainBond = "My Name is Bond, James Bond"
String secret = "jeVSnOJ2Di9c&8GA^VbX-{[Fdtm^_;"

println ("Expand key: "+ expandKey("${secret}"))

String encryptedBond = encrypt("${plainBond}", "${secret}")

println("Encrypted String: ${encryptedBond}")

String decryptedBond = decrypt("${encryptedBond}", "${secret}")

println("Decrypted String: ${decryptedBond}")


/**
 * AES Encryption
 *
 * @param plainText
 * @param secret
 * @return
 */
String encrypt(String plainText, String secret) {
    secret = expandKey(secret)
    def cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE")
    SecretKeySpec key = new SecretKeySpec(secret.getBytes("UTF-8"), "AES")
    println(new IvParameterSpec(secret.getBytes("UTF-8")).IV)
    cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(secret.getBytes("UTF-8")))

    return cipher.doFinal(plainText.getBytes("UTF-8")).encodeBase64().toString()
}

/**
 * Expand key to 16 bytes
 *
 * @param secret
 * @return
 */
def expandKey(def secret) {

    for (def i = 0; i < 4; i++) {
        secret += secret
    }

    return secret.substring(0, 16)
}

/**
 * AES Decryption
 *
 * @param cypherText
 * @param secret
 * @return
 */
def decrypt(def cypherText, def secret) {
    secret = expandKey(secret)
    byte[] decodedBytes = cypherText.decodeBase64()
    def cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE")
    SecretKeySpec key = new SecretKeySpec(secret.getBytes("UTF-8"), "AES")
    cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(secret.getBytes("UTF-8")))

    return new String(cipher.doFinal(decodedBytes), "UTF-8")
}