#!groovy
import com.contus.cd.Constant

/**
 * Build IPA
 * Bind Build information like Build number and Build date
 *
 * @param config
 * @return
 */
def call(Map config) {
    if ("${config.XCODE_PROJECT_NAME}".length() != 0) {

        // Bind Build Number and Date
        // bindBuildInfo module: "ios"

        // Copy and move the release information
        sh "cp -r releasenote.txt ${config.XCODE_PROJECT_NAME}/${config.XCODE_PROJECT_NAME}/"
        sh "mv versioninfo.txt ${config.XCODE_PROJECT_NAME}/${config.XCODE_PROJECT_NAME}/"

        // NPM install and build
        sh "apt-get install nodejs-legacy"
        sh "rm -rf node_modules/"
        sh "npm install"
        sh "npm run build:ios"

        // Remove old builds
        sh "rm -rf build/${config.XCODE_PROJECT_NAME}/*"

        // Unlock the login keychain
        sh "security set-key-partition-list -S apple-tool:,apple:,codesign: -s -k welcome@123 login.keychain-db"

        dir("${config.XCODE_PROJECT_NAME}"){
            sh "/usr/local/bin/pod update"
        }

        String environment = "${config.environment}".toLowerCase().tokenize().collect { it.capitalize() }.join('')

        // Clean and Archive the IPA
        sh "xcodebuild -scheme ${environment} -workspace ${config.XCODE_PROJECT_NAME}/${config.XCODE_PROJECT_NAME}" +
                ".xcworkspace clean archive -archivePath build/${config.XCODE_PROJECT_NAME}"
        // Sign and Export the IPA
        sh "xcodebuild -exportArchive -exportOptionsPlist ${config.XCODE_PROJECT_NAME}/${config.XCODE_PROJECT_NAME}" +
                ".plist -archivePath build/${config.XCODE_PROJECT_NAME}.xcarchive -exportPath build/${config.XCODE_PROJECT_NAME} PROVISIONING_PROFILE_SPECIFIER = \"Contus_Inhouse_Certificate_2017\""

        // Rename the IPA accordingly
        String ipaFile = "build/${config.XCODE_PROJECT_NAME}/${Constant.APP_NAME.replaceAll("\\s", "")}_${environment}_${BUILD_NUMBER}.ipa"
        sh "mv build/${config.XCODE_PROJECT_NAME}/${environment}.ipa ${ipaFile}"

        // Archive the IPA
        archiveArtifacts '**/*.ipa'

        //pushArtifacts classifier: "ios", file: "${ipaFile}", ext: "ipa", release: false
    }else{
        currentBuild.result = "FAILED"
        currentBuild.description = "Xcode Project Name cannot be empty"
        throw new hudson.AbortException("Xcode Project Name cannot be empty")
    }
}