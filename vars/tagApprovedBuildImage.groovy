#!groovy

/**
 * Tag the approved release build and push to nexus repository.
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the service name
    String imageName = "${config.imageName}".toLowerCase()
    String imageTag = ""
    String version = ""

    // Get Image Tag
    if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
        version = "${BRANCH_NAME}".replace("*/", "")
        String[] tag;
        tag = "${version}".split('release/');
        imageTag = "${tag[1]}_${BUILD_NUMBER}"
        version = "${tag[1]}"
    }

    sh "docker pull ${imageName}:${imageTag}"
    sh "docker tag ${imageName}:${imageTag} ${imageName}:${version}"
    sh "docker push ${imageName}:${version}"
}



