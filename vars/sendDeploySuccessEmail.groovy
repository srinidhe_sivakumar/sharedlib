#!groovy

/**
 * This method is used to send the Deploy success email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {
    def environment = "${config.environment}".toUpperCase()
    def emailContent = getEmailContent main: "Successfully Deployed in ${environment} Environment", sub: "${config.url}/${config.module}"

    emailext attachLog: false, body: "${emailContent}", subject: ' $PROJECT_NAME - Build #$BUILD_NUMBER - ' +
            '$BUILD_STATUS', to: "${config.to}"
}
