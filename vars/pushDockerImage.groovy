#!groovy

/**
 * Push docker image to the nexus repository
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""

    if("${BRANCH_NAME}".toLowerCase().contains("release/")){
        version = "${BRANCH_NAME}".replace("*/", "")
        String[] tag;
        tag = "${version}".split('release/');
        imageTag = "${tag[1]}_${BUILD_NUMBER}"
    }else{
        imageTag = "${BUILD_NUMBER}"
        imageName = imageName + "_dev"
    }

    sh "docker push ${imageName}:${imageTag}"
}



