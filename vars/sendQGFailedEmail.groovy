#!groovy

import com.contus.cd.Constant

/**
 * This method is used to send the SonarQube Quality gate failed email.
 *
 * @params body (to emails)
 * @return None
 */
def call(Map config) {

    emailext attachLog: false, body: getSonarFailedEmailContent("${Constant.SONAR_PROJECT_KEY}"),
            subject: '🔴 $PROJECT_NAME - Build #$BUILD_NUMBER - Static Analysis $BUILD_STATUS', recipientProviders: [[$class: 'CulpritsRecipientProvider'], [$class: 'DevelopersRecipientProvider'], [$class:'RequesterRecipientProvider']],
            to: "${config.to}"
}

/**
 * Retrieve and prepare Sonarqube failed email content.
 *
 * @param projectKey Sonarqube Project Key
 * @return Email template as string
 */


String getSonarFailedEmailContent(def projectKey) {

    def emailContent = libraryResource "templates/sonar_failed.html"
     emailContent.replace("%SUB_CONTENT%", "URL: http://192.168.10.173:9000/dashboard?id=${projectKey}");
}
