#!groovy

/**
 * Build for magento.
 * This class is not currently used.
 *
 * @param config
 * @return
 */
def call(Map config) {

    String imageName = "${config.imageName}".toLowerCase()
    String imageTag = config.imageTag

    imageName = "${imageName}:${imageTag}"

    // Create Dockerfile
    def envContent = libraryResource "${config.module}/${config.environment}"
    writeFile encoding: 'UTF-8', file: 'env', text: "${envContent}"

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "${appName}/${config.module}/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    String setup = "FROM ${imageName}\n" +
            "COPY env ${pipelineProps['pipeline.config.env.target']}\n" +
            "CMD [\"/var/www/html/shop/install.sh\"]"

    writeFile encoding: 'UTF-8', file: 'Dockerfile', text: "${setup}"

    String tempImageName = "temp:${config.module}_${imageTag}"

    sh "docker build -t ${tempImageName} --no-cache=true ."

    sh "docker tag ${tempImageName} ${imageName}"

    sh "docker rmi -f ${tempImageName}"
}

