#!groovy

/**
 * Build docker image, with the health check
 *
 * @param config
 * @return
 */
def call(Map config) {
    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""

    if ("${BRANCH_NAME}".toLowerCase().contains("release/")) {
        version = "${BRANCH_NAME}".replace("*/", "")
        String[] tag;
        tag = "${version}".split('release/');
        imageTag = "${tag[1]}_${BUILD_NUMBER}"
    } else {
        imageTag = "${BUILD_NUMBER}"
        imageName = imageName + "_dev"
    }

    def buildProps = readProperties file: 'build.properties'
    def module = "${buildProps['build.app.moduleName']}".toLowerCase()

    def appName = "iot"
    if (config.containsKey("app_name") && "${config.app_name}".length() > 0) {
        appName = "${config.app_name}"
    }

    // Create health check file in the parent directory
//    def healthcheckContent = libraryResource "${appName}/${module}/shell/healthcheck"
//    writeFile encoding: 'UTF-8', file: 'healthcheck.sh', text: "${healthcheckContent}"

    docker.withRegistry('http://nexus-versa.contus.us:8083', 'nexus-versa-credentials') {
        def builtImage = docker.build("${imageName}:${imageTag}")
        if (config.push != null && "${config.push}") {
            builtImage.push()
        }
    }
}



