#!groovy

/**
 * Build docker image, with the health check
 * @param config
 * @return
 */
def call(Map config) {
    // Get the docker name
    String imageName = "${config.imageName}".toLowerCase()
    String version = ""
    String imageTag = ""

    if ("${env.BRANCH_NAME}".toLowerCase().contains("release/")) {
        version = "${env.BRANCH_NAME}".replace("*/", "")
        String[] tag;
        tag = "${version}".split('release/');
        imageTag = "${tag[1]}_${BUILD_NUMBER}"
    } else {
        imageTag = "${BUILD_NUMBER}"
        imageName = imageName + "_dev"
    }

    def buildProps = readProperties file: 'build.properties'
    def module = "${buildProps['build.app.module']}".toLowerCase()

    def appName = " "
    if (config.containsKey("app_name") && "${config.app_name}".length() > 0) {
        appName = "${config.app_name}"
    }

    docker.withRegistry('http://192.168.8.247:8083/', '6cb5a078-45e7-48d3-bfe0-3e106d34845d')
        {
        def builtImage = docker.build("${imageName}:${imageTag}")
        if (config.push != null && "${config.push}") {
            builtImage.push()
        }
    }
}