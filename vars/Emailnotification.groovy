def call(email) {    
 String TO = "${email.to}"
 String MODULE_NAME = "${email.module}"
 String SERVICE_NAME = "${email.service}"
 String STATUS = "${email.status}" 
    mail to: "${TO}", subject: "Status of dev: ${currentBuild.fullDisplayName}", body: "${env.BUILD_URL} has result ${currentBuild.result}", BuildDetails: "module: ${MODULE_NAME}, service: ${SERVICE_NAME}, status: ${STATUS}"}
