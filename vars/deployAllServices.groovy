#!groovy

import com.cloudbees.groovy.cps.NonCPS

/**
 * Deploy All Services
 *
 * @param config
 * @return
 */
def call(Map config){

    // Load or read the resource dev properties content
    String pipelinePropText = libraryResource "all/dev.properties"
    def pipelineProps = readProperties text: "${pipelinePropText}"

    String[] modules = stringParser("${pipelineProps['pipeline.deploy.modules']}")

    def appName = "versa"
    if(config.containsKey("app_name") && "${config.app_name}".length() > 0){
        appName = "${config.app_name}"
    }

    for (int i = 0; i < modules.size(); i++) {

        String pipelineText = libraryResource "${appName}/${modules[i]}/dev.properties"
        def modulePipelineProps = readProperties text: "${pipelineText}"

        // Check and create config if not exist
        def isConfig = createDockerConfig module: "${modules[i]}", environment: "${config.ENVIRONMENT}"

        // Service name
        String SERVICE_NAME = "${pipelineProps['pipeline.deploy.appName']}_${modules[i]}"

        // Docker image name
        def DOCKER_IMAGE = "${config.DOCKER_REPO}/${SERVICE_NAME}"

        // Pull the docker image from Nexus Repository
        pullDockerImage imageName: "${DOCKER_IMAGE}", isDeploy: true, imageTag: "${config.VERSION}"

//        if ("${modules[i]}".contains("shop")) {
//            buildForMagento serviceName: "${SERVICE_NAME}", isConfigExist: isConfig, imageName:
//                    "${DOCKER_IMAGE}", module: "${modules[i]}", port: "${modulePipelineProps['dev.deploy.port']}", isDeploy:
//                    true, imageTag: "${config.VERSION}", environment: "${config.ENVIRONMENT}"
//        }

        // Create or update Docker service
        createOrUpdateDockerService serviceName: "${SERVICE_NAME}", isConfigExist: isConfig, imageName:
                "${DOCKER_IMAGE}", module: "${modules[i]}", port: "${modulePipelineProps['pipeline.deploy.port']}", isDeploy:
                true, imageTag: "${config.VERSION}", replicas: "1"
    }
}

@NonCPS
String[] stringParser(String inputString) {
    inputString.split(",")
}

