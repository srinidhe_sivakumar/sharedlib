#!groovy

/**
 * Test Declarative Pipeline with Shared library.
 *
 */
@Library('contus-cd-dev') _

pipeline {
    agent {
        node {
            label "Versa-Selenium"
        }
    }

    triggers {
        // Every 10 hours
        cron('H 0-23/10 * * *')
    }

    environment {
        MODULE = "shop"
        ENV = "verizon"
    }

    options {

        skipDefaultCheckout()

        // We'd like to make sure we only keep 50 builds at a time, so
        // we don't fill up the storage!
        buildDiscarder(logRotator(numToKeepStr: '50'))

        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 60, unit: 'MINUTES')
    }

    stages {

        stage("Auto Test") {
            tools {
                maven 'Maven 3.5.2'
            }
            steps {
                script {
                    emailList = getEmailList module: "${MODULE}"
                    runSeleniumTest module: "${MODULE}", failBuild: true, environment: "${ENV}"
                }
            }
            post {
                success {
                    sendAutoTestReportEmail to: "${emailList}", module: "${MODULE}", service: "Ecommerce",
                            status: "PASSED", environment: "${ENV}"
                }
                failure{
                    sendAutoTestReportEmail to: "${emailList}", module: "${MODULE}", service: "Ecommerce",
                            status: "FAILED", environment: "${ENV}"
                    error "Auto Test Failed"
                }
                always{
                    archiveArtifacts '**/report-*.html'
                    junit allowEmptyResults: true, testResults: 'selenium/target/surefire-reports/*.xml'
                }
            }
        }
    }
}