#!groovy

/**
 * Test Declarative Pipeline with Shared library.
 *
 */
@Library('contus-cd-dev') _

pipeline {
    agent {
        node {
            label "Versa-Selenium"
        }
    }

    triggers {
        // Every 6 hours
        cron('H 0-23/6 * * *')
    }

    environment {
        MODULE = "api"
    }

    options {

        skipDefaultCheckout()

        // We'd like to make sure we only keep 20 builds at a time, so
        // we don't fill up the storage!
        buildDiscarder(logRotator(numToKeepStr: '20'))

        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 60, unit: 'MINUTES')
    }

    stages {

        stage("Auto Test") {
            tools {
                maven 'Maven 3.5.2'
            }
            steps {
                script {
                    emailList = getEmailList module: "${MODULE}"
                    runSeleniumTest module: "${MODULE}", failBuild: true
                }
            }
            post {
                success {
                    sendAutoTestReportEmail to: "${emailList}", module: "${MODULE}"
                }
                failure{
                    sendAutoTestReportEmail to: "${emailList}", module: "${MODULE}"
                    error "Auto Test Failed"
                }
            }
        }
    }
}