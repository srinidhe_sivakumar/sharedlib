#!groovy

/**
 * Deploy Proxy Declarative Pipeline with Shared library.
 */

@Library('contus-cd-dev') _

def buildProps = ""
def module = ""

pipeline {
    agent none

    parameters {
        string(name: 'DEPLOY_NODE', defaultValue: 'Versa-Dev', description: 'Where the docker image should be ' +
                'Deployed?')
        choice(
                name: 'ENVIRONMENT',
                choices: 'demo\ndev\nqa\nuat\nverizon\nprod',
                description: 'Select Environment in which you deploy'
        )

        choice(
                name: 'APP_NAME',
                choices: 'versa\nverizon',
                description: 'Select App Name'
        )
    }

    environment {
        MODULE = 'nodepolling'
        DOCKER_REPO = 'nexus-versa.contus.us:8083'
    }

    // The options directive is for configuration that applies to the whole job.
    options {

        skipDefaultCheckout()

        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 60, unit: 'MINUTES')
    }

    stages {
        stage('Approval') {
            agent {
                node {
                    label "${DEPLOY_NODE}"
                }
            }
            steps {
                script {
                    try {
                        /**
                         * Ask for approval to proceed for Deployment.
                         */
                        timeout(time: 1, unit: 'DAYS') { //Timeout in 1 day
                            input id: 'approve_for_config_update', message: "Approve for ${params.APP_NAME}_${MODULE}" +
                                    " " +
                                    "config update in ${DEPLOY_NODE} (${ENVIRONMENT})?", ok:
                                    'Proceed', submitter: 'sathishkumar_contus,rajagopaln_contus,sampathkumar_contus'
                        }
                    } catch (err) {
                        if (err.toString().toLowerCase().contains("flowinterruptedexception")) {
                            currentBuild.result = "FAILURE"
                            error "Config update Denied"
                        }
                    }
                }
            }
        }
        stage('Update Config') {
            agent {
                node {
                    label "${DEPLOY_NODE}"
                }
            }
            steps {
                checkout scm
                script {
                    service = "${params.APP_NAME}_${MODULE}"
                    updateStackDeployServiceConfig module: "${MODULE}", environment: "${params.ENVIRONMENT}",
                            serviceName: "${service}", app_name: "${params.APP_NAME}"
                }
            }
        }

    }
}