#!groovy

/**
 * Deploy Declarative Pipeline with Shared library.
 *
 */
@Library('contus-cd-dev') _

def approval = ""

pipeline {
    agent any

    parameters {

        string(name: 'VERSION', defaultValue: '', description: 'Version of the docker image to be deployed?')
        choice(
                name: 'DEPLOY_NODE',
                choices: 'Versa-Demo\nVersa-UAT-External\nVerizon-UAT-External',
                description: 'Select Environment in which you deploy'
        )
        choice(
                name: 'ENVIRONMENT',
                choices: 'demo\nuat\nverizon\nprod',
                description: 'Select Environment in which you deploy'
        )
    }

    environment {
        APP_NAME = "versa"
        MODULE_NAME = "shop"
        HOST_PORT = "4000"
        HOST_URL = "http://205.251.66.62"
        DOCKER_REPO = 'nexus-versa.contus.us:8083'
        ENV = 'uat'
    }

    options {

        skipDefaultCheckout()

        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 60, unit: 'MINUTES')
    }

    stages {
        stage('Approval') {
            agent {
                node {
                    label "${params.DEPLOY_NODE}"
                }
            }
            steps {
                script {
                    try {
                        /**
                         * Ask for approval to proceed for Deployment.
                         */
                        timeout(time: 1, unit: 'DAYS') { //Timeout in 1 day
                            input id: 'approve_for_uat', message: "Approve for ${APP_NAME}_${MODULE_NAME}:${params.VERSION} " +
                                    "Deployment in ${DEPLOY_NODE} (${ENVIRONMENT})?", ok:
                                    'Proceed', submitter: 'sathishkumar_contus,rajagopaln_contus'
                        }
                    } catch (err) {
                        if (err.toString().toLowerCase().contains("flowinterruptedexception")) {
                            currentBuild.result = "FAILURE"
                            error "Deployment Denied"
                        }
                    }
                }
            }
        }
        stage('Deploy in UAT') {
            agent {
                node {
                    label "${params.DEPLOY_NODE}"
                }
            }
            steps {
                checkout scm
                script {

                    // Check and create config if not exist
                    def isConfig = createDockerConfig module: "${MODULE_NAME}", environment: "${params.ENVIRONMENT}"

                    // Service name
                    String SERVICE_NAME = "${APP_NAME}_${MODULE_NAME}"

                    // Docker image name
                    DOCKER_IMAGE = "${DOCKER_REPO}/${SERVICE_NAME}"

                    // Pull the docker image from Nexus Repository
                    pullDockerImage imageName: "${DOCKER_IMAGE}", isDeploy: true, imageTag: "${params.VERSION}"

//                    buildForMagento serviceName: "${SERVICE_NAME}", isConfigExist: isConfig, imageName:
//                            "${DOCKER_IMAGE}", module: "${MODULE_NAME}", port: "${HOST_PORT}", isDeploy:
//                            true, imageTag: "${params.VERSION}", environment: "${params.ENVIRONMENT}"

                    // Create or update Docker service
                    createOrUpdateDockerService serviceName: "${SERVICE_NAME}", isConfigExist: isConfig, imageName:
                            "${DOCKER_IMAGE}", module: "${MODULE_NAME}", port: "${HOST_PORT}", isDeploy:
                            true, imageTag: "${params.VERSION}", replicas: "1"

                    // Send deployment successful email
                    sendDeploySuccessEmail to: "sathishkumar@contus.in", url: "${HOST_URL}", environment: "${params.ENVIRONMENT}"

                }
            }
        }

        /**
         * Automation test
         */
        stage("Auto Test") {
            agent {
                node {
                    label "Versa-Selenium"
                }
            }
            tools {
                maven 'Maven 3.5.2'
            }
            steps {
                script {
                    runSeleniumTest module: "${MODULE_NAME}", failBuild: false, environment: "${params.ENVIRONMENT}"
                }
            }
            post {
                success {
                    sendAutoTestReportEmail to: "sathishkumar@contus.in",
                            module: "${MODULE_NAME}", service: "Ecommerce", status: "PASSED",
                            environment: "${params.ENVIRONMENT}"
                }
                failure{
                    sendAutoTestReportEmail to: "sathishkumar@contus.in",
                            module: "${MODULE_NAME}", service: "Ecommerce", status: "FAILED",
                            environment: "${params.ENVIRONMENT}"
                    error "Auto Test Failed"
                }
                always{
                    archiveArtifacts '**/report-*.html'
                    junit allowEmptyResults: true, testResults: 'selenium/target/surefire-reports/*.xml'
                }
            }
        }
    }
}