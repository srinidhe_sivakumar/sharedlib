#!groovy

/**
 * Deploy Declarative Pipeline with Shared library.
 *
 */
@Library('contus-cd-dev') _

pipeline {
    agent any

    parameters {

        choice(
                name: 'DEPLOY_NODE',
                choices: 'Versa-Demo\nVersa-UAT-External\nVerizon-UAT-External',
                description: 'Select Environment in which you deploy'
        )

    }

    environment {
        DOCKER_REPO = 'nexus-versa.contus.us:8083'
    }

    options {

        skipDefaultCheckout()

        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 60, unit: 'MINUTES')
    }

    stages {
        stage('Approval') {
            agent {
                node {
                    label "${params.DEPLOY_NODE}"
                }
            }
            steps {
                script {
                    try {
                        /**
                         * Ask for approval to proceed for Deployment.
                         */
                        timeout(time: 1, unit: 'DAYS') { //Timeout in 1 day
                            input id: 'approve_for_uat', message: "Approve for Environment " +
                                    "Setup in ${DEPLOY_NODE}?", ok:
                                    'Proceed', submitter: 'sathishkumar_contus,rajagopaln_contus'
                        }
                    } catch (err) {
                        if (err.toString().toLowerCase().contains("flowinterruptedexception")) {
                            currentBuild.result = "FAILURE"
                            error "Environment Setup Denied"
                        }
                    }
                }
            }
        }
        stage('Setup') {
            agent {
                node {
                    label "${params.DEPLOY_NODE}"
                }
            }
            steps {
                script {

                    deployAllServices ENVIRONMENT: "${params.ENVIRONMENT}", VERSION:"${VERSION}", DOCKER_REPO: "${DOCKER_REPO}"

                    // Send deployment successful email
                    sendDeploySuccessEmail to: "sathishkumar@contus.in", url: "", environment: "${params.ENVIRONMENT}"

                }
            }
        }
        /**
         * Automation test
         */
        stage("Auto Test") {
            agent {
                node {
                    label "Versa-Selenium"
                }
            }
            tools {
                maven 'Maven 3.5.2'
            }
            steps {
                script {
                    emailList = getEmailList module: "all"
                    runSeleniumTestAll environment: "${params.ENVIRONMENT}", failBuild: false
                }
            }
            post {
                success {
                    sendAutoTestReportEmail to: "${emailList}", module: "all", service: "Deploy All",
                            status: "PASSED", environment: "${params.ENVIRONMENT}"
                }
                failure{
                    sendAutoTestReportEmail to: "${emailList}", module: "all", service: "Deploy All",
                            status: "FAILED", environment: "${params.ENVIRONMENT}"
                    error "Auto Test Failed"
                }
                always{
                    archiveArtifacts '**/report-*.html'
                    //junit allowEmptyResults: true, testResults: 'selenium/target/surefire-reports/*.xml'
                }
            }
        }
    }
}