package com.contus.cd

/**
 * Constant class with has the Constant values repeatedly used in the project.
 *
 * @author sathishkumar@contus.in
 * @version 1.0
 */
class Constant implements Serializable {

    static String CREDENTIAL = "sathish_bitbucket"
    static String SONAR_PROJECT_KEY = ""


    static String NODE = ""
    static String DEV_NODE = "Versa-Demo"
    static String QA_NODE = "Versa-QA"
    static String UAT_NODE = "Versa-UAT-External"


    static String SELENIUM_REPO_URL = "https://bitbucket.org/versanetworks/svep4418-versa-rick-saas-automation"
    static String SELENIUM_REPO_BRANCH = "feature/test"

    static String MAGENTO_CONFIG_REPO_URL = "https://bitbucket.org/versanetworks/svep4418-versa-rick-web-config"
    static String SAAS_CONFIG_REPO_URL = "https://bitbucket.org/versanetworks/svep4418-versa-rick-python-config"
    static String SAAS_FRONTEND_CONFIG_REPO_URL = "https://bitbucket" +
            ".org/versanetworks/svep4418-versa-rick-saas-frontend-config"

    static String PLATFORM
    static String APP_NAME="Versa"
    static String MODULE_NAME
    static String XCODE_PROJECT_NAME

    static String CONFIG_SERVICE_NAME

    static String NEXUS_REPO = "nexus-versa.contus.us:8083"
    static String SWIFTLINT_PROPERTIES = "swiftlint.properties"

    static String STATIC_ANALYSIS_ANDROID = false

    Constant() {}
}

